const express = require('express')

const app = express()

const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.get('/home', (request, response) => {
	response.send('Welcome to the home page!')
})

let users = [{ 'username': 'johndoe', 'password': 'johndoe123' }]

app.get('/users', (request, response) => {
	response.send(users)
})

app.delete('/delete-user', (request, response) => {
	let message

	for(i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users.splice(users[i].username)

			message = `User ${request.body.username} has been deleted. `
		} else {
			message = `User does not exists`
		}
	}
	response.send(message)
})

app.listen(port, () => console.log(`Server is running at port: ${port}`))